# Copyright 2015 Thomas G. Anderson
# Distributed under the terms of the GNU General Public License v2

MY_PN="${PN}-ng"

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ] github [ user=opencollab project=${MY_PN} ]

WORK="${WORKBASE}"/${MY_PN}-${PV}

SUMMARY="A collection of Fortran77 subroutines designed to solve large scale
eigenvalue problems"
DESCRIPTION="
Important Features:

* Reverse Communication Interface.
* Single and Double Precision Real Arithmetic Versions for Symmetric,
  Non-symmetric, Standard or Generalized Problems.
* Single and Double Precision Complex Arithmetic Versions for Standard or
  Generalized Problems.
* Routines for Banded Matrices - Standard or Generalized Problems.
* Routines for The Singular Value Decomposition.
* Example driver routines that may be used as templates to implement numerous
  Shift-Invert strategies for all problem types, data types and precision.
"
HOMEPAGE="http://forge.scilab.org/index.php/p/arpack-ng/"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="mpi"

DEPENDENCIES="
    build+run:
        sys-libs/libgfortran:*
        virtual/blas
        virtual/lapack
        mpi? ( sys-cluster/openmpi )
"

BUGS_TO="tanderson@caltech.edu"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( mpi )

src_install() {
    default

    # Prune installed MPI example drivers
    if option mpi; then
        edo rm "${IMAGE}"/usr/$(exhost --target)/bin/*drv*
        edo rmdir "${IMAGE}"/usr/$(exhost --target)/bin
    fi
}

